<?php

declare(strict_types=1);

use App\Repositories\EmployeeRepository;
use App\Services\VacationCalculator;

require './vendor/autoload.php';

// Verify if the number of arguments = 3, first argument passed = --year and second argument is an valid date 
if ($argc < 3 || ($argc >= 3 && ($argv[1] != "--year" || !preg_match("/^\d{4}$/", $argv[2])))) {
    Help($argv);
    return;
} else {
    $interestYear = (int)$argv[2];
}

// Get employees from the Fixture File
$employeeRepository = new EmployeeRepository();
$employees = $employeeRepository->findAll();
foreach ($employees as $employee) {
    // Calculate vacation days for each employee
    $vacationDays = (new VacationCalculator())->setEmployee($employee)->setInterestYear($interestYear)->calculateEmployeeVacationDays();
    // Show the result: Name && Vacaton days 
    echo "\n Employee Name: " . $employee->getName() . ", Vacation days: " . $vacationDays . "\n";
}

/**
 *  This Help function will display a short description of the program
 *
 * @param array $argv
 * @return void
 */
function Help(array $argv): void
{
    # Display Help
    echo "Command line script that takes the year of interest as an input argument and outputs \nthe employees names with the respective number of vacation days.\n\n" .
        "Syntax: ${argv[0]} [--year YYYY|--help] \n" .
        "options:\n" .
        "\t--year YYYY     The year of interest.\n" .
        "\t--help          Print this Help.\n";
}
