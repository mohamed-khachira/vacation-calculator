## OOP Coding Challenge : Vacation calculator

The fictional company Ottivo wants to determine each of its employee's yearly vacation days for a given year.

### Requirements

- Each employee has a minimum of 26 vacation days regardless of age
- A special contract can overwrite the amount of minimum vacation days
- Employees >= 30 years get one additional vacation day every 5 years
- Contracts can start on the 1st or the 15th of a month
- Contracts starting in the course of the year get 1/12 of the yearly vacation days for each full month

## Built With

- PHP 7.4
- Composer

## Installation with docker & docker-compose

1- Clone Repository from GitLab

```bash
git clone https://gitlab.com/mohamed-khachira/vacation-calculator.git
```

2- Move into the folder

```bash
# cd vacation-calculator
```

3- Start containers

```bash
# make start
```

4- Install the dependencies

```bash
# docker-compose run php composer install
```

5- Run the script

```bash
# docker-compose run php make run YEAR=YYYY // example YYYY = 2001
```

6- Run the tests

```bash
# docker-compose run php make test
```

## Installation without docker & docker-compose

1- Clone Repository from GitLab

```bash
git clone https://gitlab.com/mohamed-khachira/vacation-calculator.git
```

2- Move into the folder

```bash
# cd vacation-calculator
```

3- Install the dependencies

```bash
# composer install
```

4- Run the script

```bash
# php vacation-calculator.php --year YYYY // example YYYY = 2001
```

5- Run the tests

```bash
# make test
```
