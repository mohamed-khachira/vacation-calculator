<?php

declare(strict_types=1);

namespace App\Entities;

use DateTimeInterface;

class Employee
{
    protected string $name;

    protected \DateTimeInterface $date_of_birth;

    protected \DateTimeInterface $contract_start_date;

    protected ?int $special_contract = null;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getDateOfBirth(): DateTimeInterface
    {
        return $this->date_of_birth;
    }

    public function setDateOfBirth(DateTimeInterface $date_of_birth): self
    {
        $this->date_of_birth = $date_of_birth;
        return $this;
    }

    public function getContractStartDate(): DateTimeInterface
    {
        return $this->contract_start_date;
    }

    public function setContractStartDate(DateTimeInterface $contract_start_date): self
    {
        $this->contract_start_date = $contract_start_date;
        return $this;
    }

    public function getSpecialContract(): ?int
    {
        return $this->special_contract;
    }

    public function setSpecialContract(?int $special_contract): self
    {
        $this->special_contract = $special_contract;
        return $this;
    }

    public function getAge(int $interest_year): ?int
    {
        $interest_year_start = new \DateTime(sprintf("%u-%u-%u", $interest_year, 1, 1));
        return $interest_year_start > $this->date_of_birth ? $interest_year_start->diff($this->date_of_birth)->y : null;
    }
}
