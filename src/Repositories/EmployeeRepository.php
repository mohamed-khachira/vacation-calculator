<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Entities\Employee;

class EmployeeRepository
{
    /**
     * Get an array of Employee from the Fixture File
     *
     * @return array
     */
    public function findAll(): array
    {
        $employees = [];
        $dataFixtures = include __DIR__ . '/../DataFixtures/employees.php';
        if (!empty($dataFixtures)) {
            foreach ($dataFixtures as $employeeData) {
                $employee = new Employee();
                $employee->setName($employeeData['name']);
                $employee->setDateOfBirth(new \DateTime($employeeData['date_of_birth']));
                $employee->setContractStartDate(new \DateTime($employeeData['contract_start_date']));
                $employee->setSpecialContract($employeeData['special_contract']);
                $employees[] = $employee;
            }
        }
        return $employees;
    }
}
