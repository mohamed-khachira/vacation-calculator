<?php

declare(strict_types=1);

namespace App\Services;

use App\Contracts\VacationInterface;
use App\Entities\Employee;

abstract class VacationDecorator implements VacationInterface
{
    protected VacationInterface $vacationType;

    protected Employee $employee;

    protected ?int $interestYear;

    public function __construct(VacationInterface $vacationType, Employee $employee, ?int $interestYear)
    {
        $this->vacationType = $vacationType;
        $this->employee = $employee;
        $this->interestYear = $interestYear;
    }

    abstract public function calculate(): int;
}
