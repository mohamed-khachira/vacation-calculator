<?php

declare(strict_types=1);

namespace App\Services;

class ContractInCourseTheYear extends VacationDecorator
{
    public function calculate(): int
    {
        return $this->calculateMonthsOfWorkInInterestYear($this->vacationType->calculate());
    }

    protected function calculateMonthsOfWorkInInterestYear($currentVacationDays): int
    {
        $start_year_of_work = (new \DateTime())->setDate($this->interestYear, 1, 1);
        $end_year_of_work = (new \DateTime())->setDate($this->interestYear + 1, 1, 1);

        if ($this->employee->getContractStartDate() > $end_year_of_work) {
            return 0 * $currentVacationDays;
        }

        if ($this->employee->getContractStartDate() < $start_year_of_work) {
            return 1 * $currentVacationDays;
        }

        $monthsOfWorks = $end_year_of_work->diff($this->employee->getContractStartDate())->m;

        return (int) ($currentVacationDays * $monthsOfWorks / 12);
    }
}
