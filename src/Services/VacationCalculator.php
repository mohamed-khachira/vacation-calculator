<?php

declare(strict_types=1);

namespace App\Services;

use App\Contracts\VacationCalculatorInterface;
use App\Entities\Employee;

class VacationCalculator implements VacationCalculatorInterface
{
    private Employee $employee;
    private ?int $interestYear;

    public function setEmployee(Employee $employee): self
    {
        $this->employee = $employee;

        return $this;
    }

    public function setInterestYear(int $interest_year): self
    {
        $this->interestYear = $interest_year;

        return $this;
    }

    /**
     * Calculate vacation days for each employee
     *
     * @return integer
     */
    public function calculateEmployeeVacationDays(): int
    {
        $standardVacation = new StandardVacation();
        $specialContract = new SpecialContract($standardVacation, $this->employee, null);
        $additionalVacation = new AdditionalVacation($specialContract, $this->employee, $this->interestYear);
        $contractInCourseTheYear = new ContractInCourseTheYear($additionalVacation, $this->employee, $this->interestYear);

        return $contractInCourseTheYear->calculate();
    }
}
