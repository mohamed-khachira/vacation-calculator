<?php

declare(strict_types=1);

namespace App\Services;

class AdditionalVacation extends VacationDecorator
{
    const STARTING_FROM_AGE = 30;
    const INTERVAL_YEARS = 5;

    public function calculate(): int
    {
        $additionalDays = 0;
        $employeeAge = $this->employee->getAge($this->interestYear);
        if ($employeeAge >= self::STARTING_FROM_AGE) {
            $additionalDays = floor(($employeeAge - self::STARTING_FROM_AGE) / self::INTERVAL_YEARS);
        }
        return $this->vacationType->calculate() + (int) $additionalDays;
    }
}
