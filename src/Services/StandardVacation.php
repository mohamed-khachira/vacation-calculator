<?php

declare(strict_types=1);

namespace App\Services;

use App\Contracts\VacationInterface;

class StandardVacation implements VacationInterface
{
    const STANDARD_VACATION_DAYS = 26;

    public function calculate(): int
    {
        return self::STANDARD_VACATION_DAYS;
    }
}
