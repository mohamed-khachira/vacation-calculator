<?php

declare(strict_types=1);

namespace App\Services;

class SpecialContract extends VacationDecorator
{
    public function calculate(): int
    {
        return $this->employee->getSpecialContract() ?? $this->vacationType->calculate();
    }
}
