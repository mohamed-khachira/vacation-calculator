<?php

declare(strict_types=1);

namespace App\Contracts;

interface VacationInterface
{
    public function calculate(): int;
}
