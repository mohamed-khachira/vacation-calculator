<?php

declare(strict_types=1);

namespace App\Contracts;

use App\Entities\Employee;

interface VacationCalculatorInterface
{
    public function setEmployee(Employee $employee): self;

    public function setInterestYear(int $interest_year): self;

    public function calculateEmployeeVacationDays(): int;
}
