FROM php:7.4-cli

WORKDIR /app

COPY . ./

RUN apt-get update && apt-get install -y git unzip curl

# Install composer
RUN curl -sSk https://getcomposer.org/installer | php -- --disable-tls && \
    mv composer.phar /usr/local/bin/composer

#RUN composer install

#RUN composer dump-autoload