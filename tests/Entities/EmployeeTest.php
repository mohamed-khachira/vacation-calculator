<?php

declare(strict_types=1);

namespace App\Tests\Entities;

use App\Entities\Employee;
use PHPUnit\Framework\TestCase;

class EmployeeTest extends TestCase
{
    private Employee $employee;

    protected function setUp(): void
    {
        $this->employee = new Employee();
    }

    public function testSetAndGetName(): void
    {
        $name = 'Hans Müller';
        $response = $this->employee->setName($name);
        self::assertInstanceOf(Employee::class, $response);
        self::assertEquals($name, $this->employee->getName());
    }

    public function testSetAndGetDateOfBirth(): void
    {
        $date_of_birth = new \DateTime('1950-12-30');
        $response = $this->employee->setDateOfBirth($date_of_birth);
        self::assertInstanceOf(Employee::class, $response);
        self::assertEquals($date_of_birth, $this->employee->getDateOfBirth());
    }

    public function testSetAndGetContractStartDate(): void
    {
        $contract_start_date = new \DateTime('2001-01-01');
        $response = $this->employee->setContractStartDate($contract_start_date);
        self::assertInstanceOf(Employee::class, $response);
        self::assertEquals($contract_start_date, $this->employee->getContractStartDate());
    }

    /**
     * @dataProvider vacationDaysForSpecialContract
     */
    public function testSetAndGetSpecialContract($special_contract, $expectedSpecialContract): void
    {
        $response = $this->employee->setSpecialContract($special_contract);
        self::assertInstanceOf(Employee::class, $response);
        self::assertEquals($expectedSpecialContract, $this->employee->getSpecialContract());
    }

    public function vacationDaysForSpecialContract()
    {
        return [
            [null, null],
            [27, 27]
        ];
    }

    /**
     * @dataProvider provideAgeData
     */
    public function testGetAge($date_of_birth, $interest_year, $expectedAge): void
    {
        $response = $this->employee->setDateOfBirth(new \DateTime($date_of_birth));
        self::assertInstanceOf(Employee::class, $response);
        self::assertEquals($expectedAge, $this->employee->getAge($interest_year));
    }

    public function provideAgeData()
    {
        return [
            ['date_of_birth' => '1950-12-30', 'interest_year' => 2021, 'expectedAge' => 70],
            ['date_of_birth' => '1966-06-09', 'interest_year' => 2021, 'expectedAge' => 54],
            ['date_of_birth' => '1991-07-12', 'interest_year' => 2010, 'expectedAge' => 18],
        ];
    }
}
