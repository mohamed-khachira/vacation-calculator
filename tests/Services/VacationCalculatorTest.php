<?php

declare(strict_types=1);

namespace App\Tests\Services;

use App\Entities\Employee;
use App\Services\VacationCalculator;
use PHPUnit\Framework\TestCase;

class VacationCalculatorTest extends TestCase
{
    public function testSetEmployee(): void
    {
        $response = (new VacationCalculator())->setEmployee(new Employee());
        self::assertInstanceOf(VacationCalculator::class, $response);
    }

    public function testSetInterestYear(): void
    {
        $response = (new VacationCalculator())->setInterestYear(2021);
        self::assertInstanceOf(VacationCalculator::class, $response);
    }

    /**
     * @dataProvider provideEmployees
     */
    public function testCalculateEmployeeVacationDays(Employee $employee, int $year, int $expectedVacationDays): void
    {
        $vacationCalculator = (new VacationCalculator())->setEmployee($employee)->setInterestYear($year);
        $this->assertEquals($expectedVacationDays, $vacationCalculator->calculateEmployeeVacationDays());
    }

    public function provideEmployees(): array
    {
        return [
            [
                'employee' => (new Employee())->setDateOfBirth(new \DateTime('1990-02-15')) // age based on interest year = 23
                    ->setContractStartDate(new \DateTime('2013-08-01'))->setSpecialContract(27),
                'year' => 2013,
                'expectedVacationDays' => 11,
            ],
            [
                'employee' => (new Employee())->setDateOfBirth(new \DateTime('2000-01-01')) // age based on interest year = 20
                    ->setContractStartDate(new \DateTime('2020-01-15')),
                'year' => 2020,
                'expectedVacationDays' => 23,
            ],
            [
                'employee' => (new Employee())->setDateOfBirth(new \DateTime('1980-12-31')) // age based on interest year = 37
                    ->setContractStartDate(new \DateTime('2021-09-01')),
                'year' => 2018,
                'expectedVacationDays' => 0,
            ],
            [
                'employee' => (new Employee())->setDateOfBirth(new \DateTime('1960-05-10')) // age based on interest year = 29
                    ->setContractStartDate(new \DateTime('1980-04-01')),
                'year' => 1990,
                'expectedVacationDays' => 26,
            ],
        ];
    }
}
